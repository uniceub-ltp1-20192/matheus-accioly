from Entidades.massa import Massa

class paoFrances(Massa):
  def __init__(self,tipoDeCrocrancia = "crocante"):
    super().__init__()
    self._tipoDeCrocancia = tipoDeCrocrancia

  @property
  def tipoDeRabo(self):
    return self._tipoDeCrocancia

  @tipoDeCrocrancia.setter
  def tipoDeCrocrancia(self,tipoDeCrocrancia):
    self._tipoDeCrocancia = tipoDeCrocrancia

  def assar(self):
    print('TO FICANDO GRANDE E DURO')
  
  def cozinhar(self)
    print('TO FICANDO MOLE')

  def __str__(self):
    return '''\033[1;33m
    Identificador:    {}
    Ingrediente:      {}
    Formato:          {}
    Cor:              {}
    Peso:             {}
    Tipo de Crocrancia: {}\033[m'''.format(self.identificador,self.ingrediente.title(),self.formato,self.cor,self.peso,self.tipoDeCrocrancia.title())