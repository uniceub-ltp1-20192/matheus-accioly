//No hotel morte lenta, estavam hospedados 5 hospédes, Renato Faca, Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
//A meia noite, o hóspede do quarto 101 foi encontrado morto e os 5 suspeitos são os citados acima. Descubra quem é o assassino sabendo que:
//        Se o nome do suspeito contem 3 vogais ou mais(Nome completo) e nenhuma dessas vogais é u ou o, ele pode ser o assassino.
//        Se o suspeito for do sexo feminino, só sera assassina se tiver utilizado uma arma branca.
//        Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel a 00h30 para ser considerado o assasino.
// Sabendo dessas informações acima, projete o algorítmo que fornecido o nome do suspeito, indique se ele pode ou não ser o assassino.

#include <iostream>
#include <array>
#include <string>

using namespace std;

int main()
{
	//Declarando variaveis
	string nome;
	string vogal = "a" "e" "i";
 	//Declarando o nome do suspeito
 	cout << "Digite o nome do suspeito: ";
 	cin >> nome;
 	//verificando nome dos suspeitos
 	if (nome != "Renato Faca" || "Maria Cálice" || "Roberto Arma" || "Roberta Corda" || "Uesllei")
 	{
 		cout << "ERROR 404 NAME NOT FOUND. TRY AGAIN" << endl;
 		}
 	//checando vogais
 	size_t found = nome.find(vogal);
 	if (found != string::npos)
 	{
 		cout << "Assassino confirmado. Pode prender.";
 	}


	return 0;
}